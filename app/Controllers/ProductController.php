<?php

namespace App\Controllers;
use App\Models\Category;
use App\Models\Product;
use App\Models\ShoppingCart;
class ProductController extends BaseController
{
    public function index($id): string
    {
        $category = Category::with('product')->find($id);
        $data['page'] = 'Home-Info';
        $data['title'] = 'Chi tiết sản phẩm';
        $data['product'] = $category->product;
        return view('Shop', $data);
    }
    public function info($id)
    {
        $product = Product::with('product')->find($id);
    }
    public function get_carts_not_checkout()
    {
        if(session()->has('id_user') == false)
        {
            return [];
        }
        $id_user = session('id_user');
        $data = ShoppingCart::where('ID_CS',1)->where('ID_User', $id_user)->get();
        return $data;
    }
}