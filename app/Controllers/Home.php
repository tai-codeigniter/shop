<?php

namespace App\Controllers;
use App\Models\Category;
use App\Models\Product;
use App\Models\ShoppingCart;

class Home extends BaseController
{
    public function index(): string
    {
        $db = \Config\Database::connect();
        
        $data['title'] = 'Shop';
        $data['ListShoppingCarts'] = [];
        $data['ListProducts'] = [];
        $data['titleProductList'] = "Danh sách sản phẩm";
        $data['page'] = 'Home-Daskboard';
        $data['ListProducts'] = Product::all();
        $data['ListCategories'] = Category::all();
        $totalCart = 0;
        if(session()->get('id_user'))
            $totalCart = count(ShoppingCart::where('ID_CS',1)->where('ID_User', session()->get('id_user'))->get());
        $data['totalCart'] = $totalCart;
        return view('Shop', $data);
    }
    public function views($page = 'home'){
        
    }
}