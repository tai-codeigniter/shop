<?php

namespace App\Controllers;

class AuthController extends BaseController
{
    public function index(): string
    {
        $data['title'] = "Login";
        $data['page'] = 'Login';
        return view('pages/Auth/IndexAuth', $data);
    }
    public function signup()
    {
        $data['title'] = "Signup";
        $data['page'] = 'Signup';
        return view('pages/Auth/IndexAuth', $data);
    }
   
}