<?php
function dd($data, $exit = true)
{
  echo '<pre>',
       print_r($data, true),
       '</pre>';
  if($exit === true) exit;
} 