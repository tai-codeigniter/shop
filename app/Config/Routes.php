<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
$routes->get('/', 'Home::index');
$routes->get('/Home/Search/Category/(:num)', 'ProductController::index/$1');
$routes->get('/Home/Info/(:num)', 'ProductController::info/$1');
$routes->get('/Auth/Signin', 'AuthController::index');
$routes->get('/Auth/Signup', 'AuthController::signup');